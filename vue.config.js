module.exports = {
    publicPath: '/colorui/',
    // outputDir: 在npm run build时 生成文件的目录 type:string, default:'dist'
    outputDir: 'www/',
    // pages:{ type:Object,Default:undfind } 
    productionSourceMap: false,
    // chainWebpack: config => {
    //     config.resolve
    //     .symlinks(true)
    //     return config
    // },
    // configureWebpack: config => {
    //     if (process.env.NODE_ENV === 'production') {
    //         // 为生产环境修改配置...
    //     } else {
    //         // 为开发环境修改配置...
    //     }
    // },
    css: {
        extract: true, // 是否使用css分离插件 ExtractTextPlugin
        sourceMap: false, // 开启 CSS source maps
        loaderOptions: {}, // css预设器配置项
        modules: false // 启用 CSS modules for all css / pre-processor files.
    },
    runtimeCompiler:true,
    devServer: {
        port: 9003, // 端口号
        host: '0.0.0.0',
        
        // hotOnly: false,
        // public: "tien.ali2app.com:9003",
        hot: true,
        // disableHostCheck: true,
        // inline: true,
        https: false, // https:{type:Boolean}
        // watchOptions: {
        //     poll: true
        // },
        // open: true, //配置自动启动浏览器
    }
}