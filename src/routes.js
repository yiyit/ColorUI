import HomePage from './views/home.vue';
import DynamicRoutePage from './views/home/dynamic-route.vue';
import NotFoundPage from './views/home/not-found.vue';


export default [
  {
    path: '/',
    component: HomePage,
  },
  {
    path: '(.*)',
    async(routeTo, routeFrom, resolve, reject) {
      let self = this;
      const vueComponent = () => import('@/views' + routeTo.path + '.vue');
      vueComponent().then((vc) => {
        let auth = vc.default.auth;
        let realauth = vc.default.realauth;
        resolve({ component: vc.default })
      }).catch((err) => {
        console.log(err);
      });

    }
  },
];
